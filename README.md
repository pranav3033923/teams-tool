
# TEAMS TOOL - Companion for JIRA

Tool that is used to extract relevant details related to a JIRA ticket and display that info when the user gives ticket id as the input in the command box of teams tool.

## Implementation
* User gives Issue Id in the input 
* ES Query is performed to extract information about that ticket from ElasticSearch DB (Summary, Description, Comments, creation date, creator's info etc)
* Information is displayed by framing into Adaptive cards of Microsoft Teams Framework
* String: (Summary + " " + Description) is tokenised using NLP & stemmer to extract main keywords of source ticket & IT OPS ticket (one at a time) and then score is evaluated using Jaccard index 
 * Using this score, relevant ITOPS ticket are extracted
* Above string is also sent to OpenAI for text-davinci-model-003 to get text analysis & code solution for the ticket
* A link is attached in the card of the form (<base_url>?query:base64 text) that forwards the user to ReactJS interface.
## Run Locally

Clone the project

```bash
  git clone https://link-to-project
```

Go to the project directory

```bash
  cd my-project
```

Install dependencies

```bash
  npm install
```

On VS Code, Install teams toolkit

```bash
  preview using F5 or Debug(Chrome/Edge)
```


## Screenshots

![App Screenshot](https://i.ibb.co/Bchy7Vk/Screenshot-2023-07-12-at-3-57-02-PM.png)

![App Screenshot](https://i.ibb.co/b34pgnR/Screenshot-2023-07-12-at-4-08-13-PM.png)


## DEMO VIDEO

[Teams tool - Demonstration][1]

[1]: https://sprinklr-my.sharepoint.com/:v:/p/pranav1/EaLIeXc4e5NDh2GL9cqC7R0BJaavXq6VA09xgHW-BDXy6A?e=CzZAci
## Testing

To test the tool

```bash
  Teams toolkit -> provision -> deploy
```


![Logo](https://i.ibb.co/SrVGdPY/Sprinklr-Logo-Primary-Use-Positive-RBG.jpg)

