const codechange = require("./adaptiveCards/codechange.json");
const { AdaptiveCards } = require("@microsoft/adaptivecards-tools");
const { CardFactory, MessageFactory } = require("botbuilder");
const { Configuration, OpenAIApi } = require("openai");
const spacy = require("spacy-js");
const { Client } = require("@elastic/elasticsearch");
const jaccardIndex = require("jaccard");
const natural = require("natural");

const client = new Client({
  node: "https://8e19321aa12443048cadbbd897156216.us-central1.gcp.cloud.es.io:9243",
  auth: {
    username: "elastic",
    password: "rprIfNNjtHA7v6g3zTiJ7cNs",
  },
});

// Function to preprocess text using spaCy
async function preprocessText(text) {
  const tokenResp = natural.PorterStemmer.tokenizeAndStem(text);
  return tokenResp;
}

// Function to calculate Jaccard index
function calculateJaccardIndex(setA, setB) {
  return jaccardIndex.index(setA, setB);
}

// Function to search for similar documents
async function findSimilarDocuments(index, k, sourceDoc) {
  // Preprocess the source document text
  const sourceText = `${sourceDoc._source.summary} ${sourceDoc._source.description}`;
  const sourceLemmas = await preprocessText(sourceText);
  const sourceTokens = new Set(sourceLemmas);

  // Prepare the query to fetch all documents
  const query = {
    index,
    query: {
      match: {
        projectId: "13860",
      },
    },

    size: 1000, // Set a large size to retrieve all documents (modify if needed)
  };

  // Execute the search to retrieve all documents
  const searchResults = await client.search(query);

  // Get the retrieved documents
  const retrievedDocs = searchResults.hits.hits;

  // Calculate Jaccard index for each retrieved document
  for (const doc of retrievedDocs) {
    const docText = `${doc._source.summary} ${doc._source.description}`;
    const docLemmas = await preprocessText(docText);
    const docTokens = new Set(docLemmas);
    const jaccardIndex = calculateJaccardIndex(
      Array.from(sourceTokens),
      Array.from(docTokens)
    );
    doc._score = jaccardIndex;
  }

  // Sort the retrieved documents by Jaccard index
  retrievedDocs.sort((a, b) => b._score - a._score);

  // Return the top-k similar documents
  const similarDocs = retrievedDocs.slice(0, k);

  return similarDocs;
}

const configuration = new Configuration({
  apiKey: "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",
});

async function run(ticket_id) {
  const result = await client.search({
    index: "jirastore",
    query: {
      match: {
        id: ticket_id,
      },
    },
  });

  return result.hits.hits;
}



class Codechange {
  triggerPatterns = "";

  async handleCommandReceived(context, message) {
    // verify the command arguments which are received from the client if needed.
    console.log(`App received message: ${message.text}`);

    // do something to process your command and return message activity as the response
    var tempModifiedJSON = codechange;
    var modifiedJson = tempModifiedJSON;
    // render your adaptive card for reply message
    const cardData = await run(message.text);

    // Jaccard Index & NLP implementation ---------------------------------------------------------------->
    var similarITOPS = [];
    const index = "jirastore";
    const k = 3;
    findSimilarDocuments(index, k, cardData[0])
      .then((similarDocs) => {
        console.log("Similar Documents:");
        similarDocs.forEach((doc) => {
          similarITOPS.push(doc);
        });
      })
      .catch((error) => {
        console.error("Error:", error);
      });

    const openai = new OpenAIApi(configuration);
    const responseData = await openai.createCompletion({
      model: "text-davinci-003",
      prompt:
        cardData[0]._source.description + " " + cardData[0]._source.summary,
      max_tokens: 1000,
      temperature: 0,
    });
    const responseCode = await openai.createCompletion({
      model: "text-davinci-003",
      prompt:
        cardData[0]._source.description +
        " " +
        cardData[0]._source.summary +
        ", give the code",
      max_tokens: 1000
    });

    for (const comment of cardData[0]._source.comments) {
      modifiedJson.actions[0].card.actions.push({
        type: "Action.ShowCard",
        title: `Comment is given by ${comment.authorDisplayName} (${comment.authorEmailAddress})`,
        card: {
          type: "AdaptiveCard",
          body: [
            {
              type: "TextBlock",
              text: `${comment.body}`,
              wrap: true,
            },
          ],
        },
      });
    }

    for (const attach of cardData[0]._source.attachments) {
      modifiedJson.actions[1].card.actions.push({
        type: "Action.OpenUrl",
        title: "Open Image",
        url: attach,
      });
    }
    for (const issue of cardData[0]._source.issues) {
      modifiedJson.actions[2].card.body.push({
        type: "TextBlock",
        text: `(${issue.typeOutward}) ${issue.outwardIssueKey}`,
        wrap: true,
        fontType: "Monospace",
        horizontalAlignment: "Center",
      });
    }

    modifiedJson.actions[3].card.body.push({
      type: "TextBlock",
      text: responseData.data.choices[0].text,
      wrap: true,
      fontType: "Monospace",
      horizontalAlignment: "Center",
    });
    modifiedJson.actions[4].card.body.push({
      type: "TextBlock",
      text: responseCode.data.choices[0].text,
      wrap: true,
      fontType: "Monospace",
      horizontalAlignment: "Center",
    });

    var top3ITOPSTickets = "";
    if (similarITOPS.length > 0) {
      top3ITOPSTickets = "[Most similar ITOPS tickets:";
      similarITOPS.forEach((element) => {
        top3ITOPSTickets += " Ticket id:" + element._source.id + " ";
      });
      top3ITOPSTickets += "]";
      modifiedJson.actions[2].card.body.push({
        type: "TextBlock",
        text: top3ITOPSTickets,
        wrap: true,
        fontType: "Monospace",
        horizontalAlignment: "Center",
      });
    }

    const createdDate = cardData[0]._source.created;
    const updatedDate = cardData[0]._source.updated;

    const createdDateObj = new Date(createdDate);
    const updatedDateObj = new Date(updatedDate);

    const finalCreatedDate = createdDateObj.toLocaleString();
    const finalUpdatedDate = updatedDateObj.toLocaleString();
    cardData[0]._source.created = finalCreatedDate;
    cardData[0]._source.updated = finalUpdatedDate;

    const queryParam = {
      issueId: message.text,
      similarITOPS: top3ITOPSTickets,
      GPTtext: responseData.data.choices[0].text,
      GPTcode: responseCode.data.choices[0].text,
    };
    // Convert the JSON object to a string
    const jsonString = JSON.stringify(queryParam);

    // Encode the JSON string to Base64
    const base64String = Buffer.from(jsonString).toString("base64");

    modifiedJson.body.push({
      type: "TextBlock",
      text: `[Check the interface](https://jirateams-sprinklr.web.app?query=${base64String})`,
    });
    const cardJson = AdaptiveCards.declare(modifiedJson).render(
      cardData[0]._source
    );

    return MessageFactory.attachment(CardFactory.adaptiveCard(cardJson));
  }
}

module.exports = {
  Codechange,
};
